"""
Author:         Victor Faner
Date:           2019-04-26
Description:    Module to clean and upload IPEDS surveys to
                a SQL Database
"""

import sys
import logging
import re
from pathlib import Path

import pandas as pd
import xlrd
from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema
import sqlalchemy

DB_URL = ''  # SQLAlchemy-compatible Database URL


def insert_value_labels(data, value_labels):
    """
    Replace IPEDS codes with corresponding category labels via nested
    dicts.

    :type value_labels:     pandas DataFrame
    :type data:             pandas DataFrame

    :param data:            Raw IPEDS Survey data
    :param value_labels:    IPEDS value label data dictionary

    :return:                Cleaned IPEDS table
    """
    logging.info('Creating value label dicts')
    vars_to_replace = value_labels['varname'].unique()
    var_dict = {}
    for var in vars_to_replace:
        var_dict[var] = (
            value_labels[value_labels['varname'] == var]
                .set_index('codevalue')
                ['valuelabel']  # Filter to the correct dict
                .to_dict()
        )

    # Match data codevalues to dict keys
    for var in vars_to_replace:
        data[var] = (
            data[var]
                .astype(str)
                .str.strip()
        )

    return data.replace(var_dict)


def clean_column_names(data, varlist):
    """
    Replace shortened IPEDS column names with the full variable name,
    then reformat for easier readability.

    :type data:         pandas DataFrame
    :type varlist:      pandas DataFrame

    :param data:        Raw IPEDS Survey Data
    :param varlist:     IPEDS variable list data dictionary

    :return:            Cleaned IPEDS table
    """
    logging.info('Cleaning column names')
    data = data.rename(
        varlist[['varname', 'varTitle']]
            .set_index('varname')
            ['varTitle']
            .to_dict(),
        axis=1,
    )
    data = data.rename(
        {'Unique identification number of the institution': 'UNITID'}, axis=1
    )

    cols_cleaned = (
        data.columns
            .str.replace(r'\W', ' ')
            .str.replace(r'\'', '')
            .str.replace(r'Percentage of', 'Percentage')
            .str.replace(r'percentage of', 'percentage')
            .str.replace(r'Percent of', 'Percent')
            .str.replace(r'percent of', 'percent')
            .str.replace(r'Number of', 'Number')
            .str.replace(r'number of', 'number')
            .str.replace(r'undergraduates', 'undergrad')
            .str.replace(r'undergraduate', 'undergrad')
            .str.replace(r'to students', '')
            .str.replace(r'as a', 'as')
            .str.replace(r'who are', '')
            .str.replace(r'whose', '')
            .str.replace('the institution', 'institution')
            .str.strip()
            .str.replace(r' +', '_')
    )
    data.columns = cols_cleaned

    return data


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s: %(message)s'
    )

    survey_year = sys.argv[1]
    data_dir = Path.cwd() / 'data' / survey_year
    csvs = list(data_dir.glob('*.csv'))
    xlsx = list(data_dir.glob('*.xlsx'))

    regex = re.compile(r'\d{4}|_rv|_c')  # For cleaning final table name
    table_name_dict = {
        'adm': 'Admissions_and_Test_Scores',
        'c': 'Completions',
        'gr': 'Graduation_Rates',
        'hd': 'Institutional_Characteristics',
        'sfa': 'Student_Financial_Aid_and_Net_Price',
        'f_f1a': 'Finances_Public_Institution',
        'f_f2': 'Finances_Private_Not_For_Profit_Institution'
    }

    engine = create_engine(DB_URL)

    logging.info(f'Creating IPEDS_{survey_year} schema')
    try:
        engine.execute(CreateSchema(f'IPEDS_{survey_year}'))
    except sqlalchemy.exc.ProgrammingError:
        logging.info('Schema already exists')

    # Loop through all csvs/xlsx files within directory
    for i in range(len(csvs)):
        logging.info(f'Reading {csvs[i]}')
        data = pd.read_csv(csvs[i], encoding='latin1')
        varlist = pd.read_excel(xlsx[i], 'varlist')

        try:
            value_labels = pd.read_excel(
                xlsx[i], 'Frequencies', dtype=str, usecols=[
                    'varname', 'codevalue', 'valuelabel'
                ]
            )
        except xlrd.biffh.XLRDError:
            value_labels = None  # Handle tables with no value label dictionary

        # Handle Institutional Characteristics tables
        if 'INSTNM' in data.columns:
            data = data[[
                'UNITID',
                'INSTNM',
                'ADDR',
                'CITY',
                'STABBR',
                'ZIP',
                'OPEID',
                'OPEFLAG',
                'SECTOR',
                'ICLEVEL',
                'CONTROL',
                'UGOFFER',
                'HBCU',
                'TRIBAL',
                'LOCALE',
                'NEWID',
                'OPENPUBL',
                'INSTCAT',
                'INSTSIZE',
                'LONGITUD',
                'LATITUDE',
            ]]
            value_labels = value_labels[
                value_labels['varname'].isin(data.columns)
            ]
            varlist = varlist[varlist['varname'].isin(data.columns)]
            
        logging.info('Dropping imputation flags')
        vars_to_drop = (
            varlist['imputationvar'].loc[varlist['imputationvar'].notna()]
        )
        data = data.drop(vars_to_drop, axis=1)

        if value_labels is not None:
            data = insert_value_labels(data, value_labels)

        table = clean_column_names(data, varlist)

        table_name = re.sub(regex, '', csvs[i].stem)
        table_name = table_name_dict[table_name]

        logging.info('Loading table to Database')
        table.to_sql(
            name=table_name,
            con=engine,
            schema=f'IPEDS_{survey_year}',
            if_exists='replace',
            index=False,
        )
        logging.info('Table successfully loaded to')
