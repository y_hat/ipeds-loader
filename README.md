# Description
The __create_ipeds_tables.py__ script cleans and loads IPEDS Survey data from 
the NCES (https://nces.ed.gov/ipeds/) into a user-specified SQL database. It 
automatically replaces the shorthand variable names and value codes for a given 
survey with the corresponding values from the survey dictionary.

# Requirements
This script was written in Python 3.7.3 using the libraries listed in the 
requirements.txt file.

# Currently compatible tables:

The script has successfully loaded the following tables:

* Admissions and Test Scores (ADM2017)
* Completions (C2017C)
* Graduation Rates (GR2017)
* Institutional Characteristics (HD2017)
* Student Financial Aid and Net Price (SFA1617)
* Public Institutions Finance (F1617_F1A)
* Private Not-for-profit Institutions (F1617_F2)

# Step-by-Step Instructions

1. Go to https://nces.ed.gov/ipeds/use-the-data and in the drop-down menu under 
__Survey Data__, choose __Complete data files__
2. Select a year and then click __Continue__
3. For each survey you want to pull, download both the __Data File__ AND its 
corresponding __Dictionary__.
4. Once you have all the survey and dictionary files you want to load, unzip 
them and place them all into a single folder with the survey year as the folder 
name. Move this folder into the /data/ directory of wherever the 
create_ipeds_table.py script resides.__*__
5. Open up a terminal, cd into the main directory, and then run:

```
python create_ipeds_table.py YEAR
```

Where YEAR corresponds to the name (survey year) of the folder where the surveys 
files are located, e.g.

```
python create_ipeds_table.py 2017
```

6. Go into SQL Server and verify the tables have been loaded correctly. Once
verified, go into the object explorer > __Security__ > __Schemas__, and double 
click on each schema you created (e.g. IPEDS_2014). Change the __Schema owner__ 
to dbo, if it is not already.

__*__ There may be more than one .csv in the __Data File__ folders. In these cases
choose the one with \_rv at the end (e.g. hd2017_rv.csv).

# Notes

At the end of each survey year, IPEDS provides a Provisional release of the data 
collected. Institutions have ~9 months to correct this release, meaning final 
release data is generally not available until ~2 years after the original survey
year. If loading in provisional data, add the word \_provisional to the end of the
year when naming the survey folder.
